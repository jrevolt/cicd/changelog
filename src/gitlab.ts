import {rethrow, xrequest} from "./util";

export interface GitlabRelease {
  name: string
  tag_name: string
  tag_message: string
  ref: string
  description: string
  assets: {
    links: GitlabReleaseLink[],
  }
}

export interface GitlabReleaseLink {
  id?: number
  name: string
  url: string
  external?: boolean
  link_type?: string
}

export class Gitlab {

  options = {}

  constructor(url: URL, user, pass, token) {
    this.options = {
      host: url.hostname,
      port: url.port,
      protocol: url.protocol,
      path: url.pathname,
      headers: {
        'authorization': (user && pass) ? `Basic ${btoa(`${user}:${pass}`)}` : (token) ? `bearer ${token}` : '',
        "content-type": "application/json",
        "accept": "application/json",
      }
    }
  }

  get = async (path) => xrequest(this.options, 'GET', path, null)
  post = async (path, data) => xrequest(this.options, 'POST', path, data)
  put = async (path, data) => xrequest(this.options, 'PUT', path, data)

  async getRelease(project, tag) : Promise<GitlabRelease> {
    let x = await this.get(`projects/${encodeURIComponent(project)}/releases/${tag}`)
      .catch(e => 404 === e.statusCode ? undefined : rethrow(e))
    return x as GitlabRelease
  }

  async createRelease(project, tag, sha, message, description, links: GitlabReleaseLink[]) {
    await this.post(`projects/${encodeURIComponent(project)}/releases`, <GitlabRelease>{
      tag_name: tag,
      tag_message: message,
      ref: sha,
      description: description,
      assets: {
        links: [...links],
      },
    })
  }

  async updateRelease(project, tag, description, links: GitlabReleaseLink[]) {
    await this.put(`projects/${encodeURIComponent(project)}/releases/${tag}`, <GitlabRelease>{
      description: description,
      assets: {
        links: [...links],
      },
    })
  }

  async makeRelease(project, tag, sha, message, description, links: GitlabReleaseLink[]) {
    let r = await this.getRelease(project, tag)
    if (!r)
      await this.createRelease(project, tag, sha, message, description, links)
    else
      await this.updateRelease(project, tag, description, links)
  }

}
