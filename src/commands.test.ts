import {changelog} from "./commands";


describe('commands', () => {
  test('changelog', () => {
    changelog({
      project: 'cicd/demo/changelog',
      path: '../demo/changelog',
      tag: '0.6.0',
      ref: 'HEAD',
    })
  })
})
