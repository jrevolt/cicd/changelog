import {rethrow, xrequest} from "./util";

export interface JiraIssue {
  key: string
  fields: {
    summary: string
    description: string
    issuetype: {
      id: number
      name: string
      subtask: boolean
    }
  }
}

export class Jira {

  options = {}

  constructor(url: URL, user?, pass?, token?) {
    this.options = {
      host: url.hostname,
      port: url.port,
      protocol: url.protocol,
      path: url.pathname,
      headers: {
        'authorization': (user && pass) ? `Basic ${btoa(`${user}:${pass}`)}` : (token) ? `bearer ${token}` : '',
        "content-type": "application/json",
        "accept": "application/json",
      }
    }
  }

  get = async (path) => await xrequest(this.options, 'GET', path, null)

  async getIssue(issue) : Promise<JiraIssue> {
    let x = await this.get(`rest/api/2/issue/${issue}`)
      .catch(e => 404 === e.statusCode ? undefined : rethrow(e))
    return x as JiraIssue
  }

}
