import {Git} from "./git";
import {Jira, JiraIssue} from "./jira";
import {dbg, fail, init} from "./util";
import {Gitlab} from "./gitlab";
import semverPrerelease from "semver/functions/prerelease";

init()

export async function changelog(opts : {
  path?: string
  tag?: string
  ref?: string
  preReleases?: boolean|string
  project?: string
  gitlabUrl?: string
  gitlabUser?: string
  gitlabPass?: string
  gitlabToken?: string
  jiraUrl?: string
  jiraUser?: string
  jiraPass?: string
  jiraToken?: string
}) {

  opts.path || fail()

  if (opts.preReleases) {
    opts.preReleases = /true/i.test(opts.preReleases as string)
  } else {
    opts.preReleases = !!semverPrerelease(opts.tag)
  }

  let result : any = {}

  dbg("Args: %s", opts)

  dbg('Processing git repository %s', opts.path)

  let git = new Git(opts.path!)
  let sha = await git.resolveRefToCommit(opts.ref ?? 'HEAD') ?? fail()
  let isRefTagged = false

  let tags = await git.tags(sha, opts.preReleases)
  tags = tags.reverse().filter(x => x !== opts.tag)

  let since
  if (isRefTagged && tags.length > 1) since = tags[1]
  if (!isRefTagged && tags.length > 0) since = tags[0]
  let range = since ? `${since}..${sha}` : sha

  dbg('Loading Git history %s', range)
  let commits = await git.load(range)
  let issueIds = git.extractJiraIssues(commits)
  dbg('Git history reports following Jira issues: %s', issueIds)

  result = {
    ...result,
    git: issueIds,
  }

  opts.jiraUrl || fail('jiraUrl?')
  opts.jiraUser && opts.jiraPass || opts.jiraToken || fail('jiraUser+jiraPass? / jiraToken?')

  dbg('Resolving Jira issues from %s', opts.jiraUrl)
  let jira = new Jira(new URL(opts.jiraUrl!), opts.jiraUser, opts.jiraPass, opts.jiraToken)
  let issues : Map<string, JiraIssue> = new Map()
  await issueIds.forEachAsync(async (x) => {
    //x = 'DEVOPS-116' //qdh
    if (issues.has(x)) return
    let found = await jira.getIssue(x)
    if (found) issues.set(x, found)
    else dbg('Issue not found: %s. Ignoring', x)
  })

  result = {
    ...result,
    jira: {},
  }
  issues.forEach(x => {
    result.jira[x.key] = x.fields.summary
  })

  let desc = ""
  {
    let values = Array.from(issues.values())
    values
      .sort((x,y) => y.fields.issuetype.id - x.fields.issuetype.id)
      .sort((x,y) => y.key.localeCompare(x.key))
      .map(x => x.fields.issuetype.name)
      .filter((v,i,a)=>a.indexOf(v)==i) // distinct
      .forEach(type => {
      desc += `### ${type}\n`
      values.filter(x => x.fields.issuetype.name == type).forEach(x => {
        desc += `- [${x.key}](${opts.jiraUrl}/browse/${x.key}) ${x?.fields.summary}\n`
      })
    })
    let sinceSha = since ? await git.resolveRefToCommit(since) : undefined
    desc+=`\n(history since ${since ?? "beginning"}, or ${sinceSha ?? ""}...${sha})`
  }

  result = {
    ...result,
    changelog: {},
  }
  issues.forEach(x => {
    let c = result.changelog[x.fields.issuetype.name] ??= []
    c.push(x.key)
  })

  if (opts.project) {

    opts.gitlabUrl || fail('gitlabUrl?')
    opts.gitlabUser && opts.gitlabPass || opts.gitlabToken || fail('gitlabUser? gitlabPass? / gitlabToken?')

    let gitlab = new Gitlab(new URL(opts.gitlabUrl!), opts.gitlabUser, opts.gitlabPass, opts.gitlabToken)
    await gitlab.makeRelease(opts.project, opts.tag, sha, 'release', desc,[])
    result = {
      ...result,
      release: {
        tag: opts.tag,
        description: desc,
      }
    }
  }

  console.log(JSON.stringify(result, null, 2))
}
