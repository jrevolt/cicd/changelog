import * as http from "http";
import * as https from "https";
import * as util from "util";

export function init() {}

export function fail<T>(msg ?: string) : never {
  throw Error(msg)
}

export function dbg(msg, ...args) {
  console.warn(util.format(msg, ...args))
}

declare global {
  interface Array<T> {
    forEachAsync(action: (item:T)=>Promise<any>) : Promise<any>
  }
}

Array.prototype.forEachAsync = async function (cb : (x:any) => Promise<void>) {
  await Promise.all(this.map(async (x) => cb(x)));
};


export function xrequest(target, method, path, data) {
  let client = target.protocol === 'https:' ? https : http;
  let fqpath = `${target.path}/${path}`.replace(/\/+/, "/")
  return new Promise((resolve, reject) => {
    let opts = Object.assign({}, target, {
      method: method ?? (data ? 'POST' : 'GET'),
      path: fqpath,
    })
    dbg(`%s %s`, method, fqpath)
    if (data) dbg(JSON.stringify(data))
    let req = client.request(opts, (res) => {
      let body : any = []
      res.on('data', (chunk) => { body.push(chunk) })
      // resolve on end
      res.on('end', () => {
        try {
          body = JSON.parse(Buffer.concat(body).toString())
        } catch(e) {
          reject(e)
        }
        if (res.statusCode && res.statusCode == 404) return reject(res)
        if (res.statusCode && (res.statusCode < 200 || res.statusCode >= 300)) {
          return reject(new Error(`${res.statusCode}/${res.statusMessage}\n${JSON.stringify(body)}`))
        }
        resolve(body)
      })
      res.on('error', (err) => {
        reject(err)
      })
    })
    req.on('error', (err) => {
      reject(err)
    })
    if (data) { req.write(JSON.stringify(data)) }
    req.end()
  })
}

export function rethrow(err) : never {
  throw new Error(err.message, err);
}





