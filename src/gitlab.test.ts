import {Gitlab} from "./gitlab";

describe('gitlab', () => {

  let gitlab = new Gitlab(new URL('http://localhost:8080/api/v4/'), null, null, process.env.GITLAB_TOKEN)

  test('release: get', async () => {
    await gitlab.getRelease('cicd/demo/changelog', '0.1.0-missing')
  })

  test('release: create', async () => {
    // await gitlab.createRelease('cicd/demo/changelog', '0.3.0', '# hey',[{
    //   name: 'changelog',
    //   url: 'http://example.com',
    // }])
  })

  test('release: make', async () => {
    // await gitlab.createRelease('cicd/demo/changelog', '0.4.0', '# hey',[{
    //   name: 'changelog',
    //   url: 'http://example.com',
    // }])
  })
})
