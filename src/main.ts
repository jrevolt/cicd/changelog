import {program} from "commander";
import {changelog} from "./commands";

function wrap(action) {
  return async function() {
    await action.apply(null, arguments)
  }
}

program
  .command('changelog')
  .option('--path <path>')
  .option('--ref <ref>')
  .option('--tag <tag>')
  .option('--project <project>')
  .option('--pre-releases <preReleases>')
  .option('--jira-url <url>')
  .option('--jira-user <user>')
  .option('--jira-pass <pass>')
  .option('--jira-token <token>')
  .option('--gitlab-url <url>')
  .option('--gitlab-user <user>')
  .option('--gitlab-pass <pass>')
  .option('--gitlab-token <token>')
  .action(wrap(changelog))

program.parse()
