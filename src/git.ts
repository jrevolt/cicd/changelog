import gitlog from "gitlog"

import {exec, execSync, spawnSync} from "child_process";

import semverSort from "semver/functions/sort";
import semverValid from "semver/functions/valid";
import semverPrerelease from "semver/functions/prerelease";

export interface GitLogEntry {
  hash: string
  rawBody: string
}

export class Git {

  repo: string

  constructor(repo: string) {
    this.repo = repo;
  }

  async load(range: string) : Promise<GitLogEntry[]> {
    let commits = gitlog({
      repo: this.repo,
      number: -1,
      branch: range,
      fields: [ "hash", "committerDate", "rawBody" ],
    })
    return commits
  }

  async tags(commit, includePrerelease = true) : Promise<string[]> {
    let tags = execSync(`git tag --merged ${commit}`, {cwd: this.repo})
      .toString()
      .trim()
      .split('\n')
    tags = semverSort(tags
      .filter(x => semverValid(x))
      .filter(x => includePrerelease || !semverPrerelease(x))
    )
    return tags
  }

  async getRefTag(ref) : Promise<string|null> {
    try {
      let tag = execSync(`git describe --exact-match ${ref}`, {cwd: this.repo}).toString().trim()
      return tag && tag.length > 0 ? tag : null
    } catch (e) {
      return null
    }
  }

  async resolveRefToCommit(tag) : Promise<string|null> {
    let commit = execSync(`git rev-list ${tag} -n1`, {cwd: this.repo}).toString().trim()
    return commit && commit.length > 0 ? commit : null
  }

  extractJiraIssues(entries : GitLogEntry[]) : string[] {
    let issues : string[] = []
    const regex = /[A-Z]+-[0-9]+/gm
    entries.forEach(x => {
      let m = x.rawBody.match(regex)
      if (m) issues.push(...m as string[])
      return
    })
    issues = [...new Set(issues)] // filters out duplicates
    return issues
  }

}


