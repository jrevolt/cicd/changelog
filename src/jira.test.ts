import {Jira} from "./jira";

describe('jira', () => {

  let jira = new Jira(new URL('https://mga-sk.atlassian.net:443'), process.env.JIRA_USER, process.env.JIRA_PASS, process.env.JIRA_TOKEN)

  test('issue', async () => {
    let issue = await jira.getIssue('DEVOPS-116')
    return issue
  })
})
