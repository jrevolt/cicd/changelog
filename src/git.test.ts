import {Git} from "./git";

describe('git', () => {

  let git = new Git('test/repo1')

  test('load', async () => {
    let git = new Git('test/repo1')
    let x = await git.load('HEAD')
    return
  })
  test('tags', async () => {
    let x = await git.tags('HEAD')
    return
  })
  test('head-tag', async () => {
    let x = await git.getRefTag('HEAD')
    return
  })
  test('jiraIssues', async () => {
    let commits = await git.load('HEAD')
    let issues = git.extractJiraIssues(commits)
  })
})
