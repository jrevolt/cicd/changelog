#syntax=docker/dockerfile:1.2

FROM alpine as files
WORKDIR /app
ADD . ./
RUN if [ -f gitversion.json ]; then mv gitversion.json src/; fi

FROM node:lts-alpine
RUN apk add git && npm install -g ts-node
WORKDIR /app
ADD package*json ./
RUN npm ci -d
COPY --from=files /app ./
ENTRYPOINT [ "ts-node", "src/main.ts" ]

