#!/usr/bin/env bash
set -eu

build() {
  gitversion > gitversion.json
  docker compose build || return 1
  "$@"
}

console() {
  local opts=()
  [ -n "${entrypoint:-}" ] && opts+=("--entrypoint=$entrypoint")
  docker compose run -it --rm "${opts[@]}" main "$@"
}

"$@"
