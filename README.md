
# purpose

generate end user changelog from git history and referenced jira issues

# concept

- retrieve git commit history since previous release
- extract jira issue references from git log messages
- dereference jira issues, if needed
  - e.g. use parent issues for subtasks
  - or, use epics for issues with missing changelog metadata
  - or, process linked issues to find eligible candidate
- filter out unwanted issues:
  - by metadata: missing required annotations / labels / custom fields
  - by explicit ignore labels
  - by priority: issue is below specified threshold
- resolve changelog entry's subject / title
  - prefer dedicated jira custom field
  - fallback to jira issue summary
- generate changelog (json)
  - upload to nexus
- create gitlab release
  - content: changelog in markdown
  - assets: attach changelog as json
- use changelog:
  - gitlab project UI: releases

## git commit log

typical entry:

```
JIRA-123,JIRA-234 JIRA-345 summary

detailed description, JIRA-999

+semver: feature
```

parsing: https://regex101.com/r/obSRqb/2

---

# roadmap

## v1

uvodna implementacia najjednoduchsieho end-to-end scenara
- pipeline integration
- gitlab release

## v2

kategorizacia
- podla typu issue
- podla jira label

## v3



## jira issue metadata

- labels
- custom fields
- priority

## jira issue links





## dereferencing

- subtasks:
  - parent issue is considered instead
- bugs:
  - used AS IS
- standard issues:
  - if properly annotated, use AS IS
  - otherwise, look for an associated epic




---

## process

- resolve git commit log
  - since latest tag
  - only annotated tags are considered
  - current / top level / head tag is ignored
- extract jira issue IDs from git commit log
- fetch issue details from Jira
- resolve changelog issues:
  - only specifically labeled / flagged issues are considered
  - if issue is not eligible, check all its references
    - parent issue
    - epics
    - issue links (only selected relationships)

## changelog category

- `ChangelogCategory` custom jira issue field

## changelog entry

- Jira issue ID
- Jira issue link/reference
- `ChangelogSubject` custom jira issue field (if undefined, falls back
  to jira issue summary)


