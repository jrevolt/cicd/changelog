# 1.2.3 (2022-01-31)

## changes

- [JIRA-123](https://jira/issue/JIRA-123) REST API now requires client certificate

## new features

- [JIRA-123](https://jira/issue/JIRA-123) user profile: custom avatar

## bug fixes

- [JIRA-123](https://jira/issue/JIRA-123) fixed email validation error in user profile
